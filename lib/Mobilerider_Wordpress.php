<?php
/**
 * Main class of the plugin. Handles all the interaction with Wordpress.
 */
class Mobilerider_Wordpress
{
  /**
   * Connects the Wordpress hooks.
   */
  public function __construct()
  {
    add_action('admin_menu', array($this, 'admin_menu'));
    add_action('admin_init', array($this, 'admin_init'));
    add_action('init', array($this, 'register_js_css'));

    add_shortcode(MOBILERIDER_SHORTCODE, array($this, 'shortcode'));
  }

  /**
   * Registers the settings of the plugin
   */
  public function admin_init()
  {
    register_setting(MOBILERIDER_PREFIX . 'settings', MOBILERIDER_PREFIX . 'app_id');
    register_setting(MOBILERIDER_PREFIX . 'settings', MOBILERIDER_PREFIX . 'app_secret');

    add_settings_section(MOBILERIDER_PREFIX . 'api_settings', 'API Settings', 
                         array($this, 'settings_section_api_settings'), MOBILERIDER_PREFIX . 'settings_');
    add_settings_field(MOBILERIDER_PREFIX . 'app_id_field', 'Application ID', 
                       array($this, 'settings_field_app_id'), MOBILERIDER_PREFIX . 'settings_', 
                       MOBILERIDER_PREFIX . 'api_settings');
    add_settings_field(MOBILERIDER_PREFIX . 'app_secret_field', 'Application Secret', 
                       array($this, 'settings_field_app_secret'), MOBILERIDER_PREFIX . 'settings_', 
                       MOBILERIDER_PREFIX . 'api_settings');
  }

  public function admin_menu()
  {
    add_options_page('Mobilerider Settings', 'Mobilerider', 'manage_options',
                     MOBILERIDER_PREFIX . 'settings', array($this, 'admin_options_page'));
  }

  public function admin_options_page()
  {
?>
<div class="wrap">
  <h2>Mobilerider Settings</h2>

	<form method="post" action="options.php">
		<?php settings_fields(MOBILERIDER_PREFIX . 'settings'); ?>
    <?php do_settings_sections(MOBILERIDER_PREFIX . 'settings_'); ?>
		<p class="submit"><input type="submit" class="button-primary" value="<?php _e('Save Changes'); ?>"  /></p>
	</form>
</div>
<?php
  }

  public function settings_section_api_settings()
  {
?>
<div class="message">
  To get an application id and application secret key go <a href="http://api.mobilerider.com/" target="_blank">here</a>
</div>
<?php
  }

  public function settings_field_app_id()
  {
    $value = get_option(MOBILERIDER_PREFIX . 'app_id');
?>
<input type="text" name="<?php echo MOBILERIDER_PREFIX . 'app_id'; ?>" id="<?php echo MOBILERIDER_PREFIX . 'app_id'; ?>" value="<?php echo $value; ?>" />
<?php
  }

  public function settings_field_app_secret()
  {
    $value = get_option(MOBILERIDER_PREFIX . 'app_secret', '');
?>
<input type="text" name="<?php echo MOBILERIDER_PREFIX . 'app_secret'; ?>" id="<?php echo MOBILERIDER_PREFIX . 'app_secret'; ?>" value="<?php echo $value; ?>" />
<?php
  }

  public function shortcode($attributes)
  {
    $app_id = get_option(MOBILERIDER_PREFIX . 'app_id');
    $app_secret = get_option(MOBILERIDER_PREFIX . 'app_secret');
    if($app_id == '' || $app_secret == '')
      return '<span>Application needs to be set up.</span>';

    require_once(MOBILERIDER_DIR . 'lib/api-client/src/Mobilerider.php');
    $api_client = new Mobilerider($app_id, $app_secret);
    $app_object = $api_client->getApplication();

    $api_params = array();
    $player_params = array('vendor' => $app_object->vendorid, 
                           'playerContainer' => '.mobilerider-player-container',
                           'autoplay' => true);
    foreach($attributes as $key => $value) {
      switch($key) {
      case 'limit':
        $api_params[$key] = $value;
        break;
      case 'width':
        $player_params['width'] = $value;
        break;
      case 'height':
        $player_params['height'] = $value;
        break;
      case 'autoplay':
        $player_params['autoplay'] = $value;
        break;
      case 'skin':
        $player_params['skin'] = $value;
        break;
      case 'mute':
        $player_params['mute'] = $value;
      case 'carousel':
        $player_params['carousel'] = (bool)$value;
        break;
      case 'carouselvisible':
        $player_params['carouselVisibleCount'] = $value;
        break;
      case 'channel':
        $api_params['channel'] = $value;
        break;
      case 'carouselvertical':
        $player_params['carouselVertical'] = (bool)$value;
        break;
      default:
        break;
      }
    }

    $playlist = $api_client->getMedia($api_params);
?>
<div class="mobilerider-wrapper">
<div class="mobilerider-player-container"></div>
<div class="mobilerider-current-container">
  <div class="mobilerider-current-title"></div>
  <div class="mobilerider-current-description"></div>
</div>
<div class="mobilerider-playlist-container<?php if($player_params['carousel'] == true) { ?> jcarousel<?php } ?>">
<?php if(count($playlist) == 0) { ?>
  <div class="mobilerider-empty-playlist">Playlist empty</div>
<?php } else { ?>
  <ul class="mobilerider-playlist">
        <?php foreach($playlist as $item) { if($item->type == 'Live Video') continue; ?>
    <li class="mobilerider-playlist-item mobilerider-item mobilerider-type-<?php echo strtolower(str_replace(' ', '', $item->type)); ?>" id="mobilerider-id-<?php echo $item->id; ?>">
      <a href="#">
        <img src="<?php echo $item->thumbnails->medium; ?>" />
        <span class="mobilerider-item-title"><?php echo $item->title; ?></span>
        <span class="mobilerider-item-description"><?php echo $item->description; ?></span>
        <span class="mobilerider-item-metatags"><?php echo $item->metatags; ?></span>
      </a>
    </li>
<?php } ?>
  </ul><!-- .mobilerider-playlist -->
  <script>
      jQuery(document).ready(function($) {
          var options = <?php echo json_encode((object)$player_params); ?>;
          var mobileriderPlayback = new MobileriderPlayback(options);
      });
  </script>
<?php
    }
?>
</div><!-- .mobilerider-playlist-container -->
<?php if($player_params['carousel'] == true) { ?>
<div class="mobilerider-playlist-controls">
  <div class="mobilerider-playlist-control mobilerider-playlist-prev"></div>
  <div class="mobilerider-playlist-control mobilerider-playlist-next"></div>
</div><!-- .mobilerider-playlist-controls -->
</div>
<?php } ?>
<?php
  }

  /**
   * Hook for init to insert required css and js files.
   */
   function register_js_css()
   {
    wp_register_style(MOBILERIDER_PREFIX . '_styles', MOBILERIDER_URL . 'css/styles.css');
		wp_enqueue_style(MOBILERIDER_PREFIX . '_styles');

    wp_enqueue_script('jquery');
    wp_enqueue_script(MOBILERIDER_PREFIX . '_player', 
                      'http://store.mobilerider.com/libs/mobilerider/mobilerider.nojquery.min.js', 
                      array('jquery'), 1);
    wp_enqueue_script(MOBILERIDER_PREFIX . '_jcarousel',
                      'http://bitcast-r.v1.sjc1.bitgravity.com/mobilerider/assets/js/jquery.jcarousel.min.js',
                      array('jquery'), '1');
    wp_enqueue_script(MOBILERIDER_PREFIX . '_playback', 
                      MOBILERIDER_URL . 'js/MobileRiderPlayback.js', 
                      array('jquery', MOBILERIDER_PREFIX . '_player', MOBILERIDER_PREFIX . '_jcarousel'), '1');

  }
}
?>
