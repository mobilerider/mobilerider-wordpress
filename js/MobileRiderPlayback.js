var MobileriderPlayback;
(function($) {

MobileriderPlayback = function() {
    MobileriderPlayback.instance = this;
    if(arguments.length > 0) {
        this.options = $.extend({}, this.options, arguments[0]);
    }
    if((parseFloat(this.options.autoplay) == parseInt(this.options.autoplay)) && !isNaN(this.options.autoplay)) {
        this.options.autoplayCount = this.options.autoplay;
        this.options.autoplay = this.options.autoplayCount > 0;
    }
    this.playedCount = 0;
    this.playlistContainer = $(this.options.playlistContainer);
    this.playlistElements = this.playlistContainer.find('.mobilerider-playlist-item');
    var currentContainer = $('.mobilerider-current-container');
    this.currentTitleElement = currentContainer.find('.mobilerider-current-title');
    this.currentDescriptionElement = currentContainer.find('.mobilerider-current-description');
    this.playerId = 'mobilerider-player-' + (new Date()).getTime();
    var playerContainer = $(this.options.playerContainer);
    this.initialized = (this.playlistElements.length > 0 && playerContainer.length > 0);
    $('<div id="' + this.playerId + '"></div>').appendTo(playerContainer);

    var this_ = this;

    if(this.options.carousel) {
        if(!this.options.carouselVertical) {
            var width = $(this.playlistElements[0]).outerWidth() * this.playlistElements.length;
            this.playlistContainer.find('ul:first').css('width', width);
        } else {
            var height = $(this.playlistElements[0]).outerHeight() * this.playlistElements.length;
            this.playlistContainer.find('ul:first').css('height', height);
        }
        this.playlistContainer.jcarousel({wrap: 'circular', vertical: this.options.carouselVertical});
        var controlsContainer = $('.mobilerider-playlist-controls');
        controlsContainer.delegate('.mobilerider-playlist-control', 'click',
                                   function(event) { return this_.onPlaylistControlClicked(event, this); });
    }

    this.playlistContainer.delegate('li.mobilerider-playlist-item a', 'click', 
                                    function() { return this_.onPlaylistItemClicked(this); });
    this.currentActive = null;
    this.next();
};

MobileriderPlayback.instance = null;

MobileriderPlayback.prototype.options = {
    skin: "main",
    autoplay: true,
    autoplayCount: -1,
    vendor: null,
    playerContainer: '',
    playlistContainer: '.mobilerider-playlist-container',
    width: 640,
    height: 360,
    mute: false,
    carousel: false,
    carouselVisibleCount: 4,
    carouselVertical: false
};

MobileriderPlayback.prototype.next = function() {
    if(!this.initialized)
        return;

    if(this.options.autoplayCount != -1 && this.options.autoplayCount == this.playedCount)
        return;

    if(this.currentActive == null) {
        this.currentActive = $('.mobilerider-playlist-item-active');
        this.currentActive = $(this.playlistElements[0]);
    } else {
        this.currentActive.removeClass('mobilerider-playlist-item-active');
        this.currentActive = this.currentActive.next();
        if(this.currentActive.length == 0) {
            this.currentActive = null;
            return;
        }
    }

    this.play();
};

MobileriderPlayback.prototype.playYoutube = function() {
    var metatags = this.currentActive.find('.mobilerider-item-metatags').text();
    var pattern = /youtube=(\S+)/g;
    var result = pattern.exec(metatags);
    var autoplay = (this.options.autoplay ? '1' : '0');
    if(result) {
        var ytUrl = 'http://www.youtube.com/v/' + result[1];
        ytUrl += '?enablejsapi=1&playerapiid=' + this.playerId;
        ytUrl += '&autoplay=' + autoplay + '&version=3';
        swfobject.embedSWF(ytUrl, this.playerId, this.options.width, 
                           this.options.height, "8", null, null, 
                           {allowScriptAccess: 'always'}, {id: this.playerId});
    } else {
        this.playedCount++;
        this.next();
    }
};

MobileriderPlayback.prototype.play = function() {
    if(!this.initialized)
        return;

    var itemId;
    if(arguments.length == 0) {
        itemId = this.currentActive.attr('id').split('-')[2];
    } else {
        var element = $(arguments[0]);
        if(element.length > 0) {
            this.currentActive = element;
            itemId = this.currentActive.attr('id').split('-')[2];
        } else {
            itemId = arguments[0];
            this.currentActive = this.playlistElements.find('#mobilerider-id-' + itemId);
        }
    }
    
    this.currentTitleElement.html(this.currentActive.find('.mobilerider-item-title').html());
    this.currentDescriptionElement.html(this.currentActive.find('.mobilerider-item-description').html());
    this.currentActive.addClass('mobilerider-playlist-item-active');
    var autoplay = (this.options.autoplay ? '1' : '0');
    var mute = (this.options.mute ? '1' : '0');
    var playerType, extrasValue, serviceValue;
    if(this.currentActive.hasClass('mobilerider-type-videos')) {
        playerType = 'osmf';
        extrasValue = 'skin:' + this.options.skin + ',continuous:1,vs:0,muteOn:0,autoplay:' + autoplay;
        serviceValue = 1;
    } else if(this.currentActive.hasClass('mobilerider-type-livevideo') || 
       this.currentActive.hasClass('mobilerider-type-videosegment')) {
        playerType = 'osmflive';
        extrasValue = 'skin:' + this.options.skin + '-live';
        extrasValue += ',continuous:1,vs:0,muteOn:0,live:1,showArchive:1,autoplay:' + autoplay;
        serviceValue = 2;
    }
    mobilerider.embedVideo(this.options.vendor, itemId, this.playerId, 
                           this.options.width, this.options.height, playerType, 
                           {extras: extrasValue, service: serviceValue});
};

MobileriderPlayback.prototype.onPlaylistItemClicked = function(element) {
    this.play($(element).closest('li.mobilerider-playlist-item'));
    return false;
};

MobileriderPlayback.prototype.onPlaylistControlClicked = function(event, element) {
    var el = $(element);
    var change = el.hasClass('mobilerider-playlist-prev') ? '-' : '+';
    this.playlistContainer.jcarousel('scroll', change + '=' + this.options.carouselVisibleCount);
    return false;
};

})(jQuery);

function getNextMedia() {
    MobileriderPlayback.instance.playYoutube();
    return 1;
}

function onYouTubePlayerReady(playerId) {
    var player = document.getElementById(playerId);
    player.addEventListener('onStateChange', 'onYouTubePlayerStateChange');
    if(MobileriderPlayback.instance.options.mute)
        player.mute();
}

function onYouTubePlayerStateChange(newState) {
    if(newState == 0)
        MobileriderPlayback.instance.next();
}
