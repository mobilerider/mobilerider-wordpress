<?php
/*
Plugin Name: MobileRider Integration
Plugin URI: https://bitbucket.org/mobilerider/mobilerider-wordpress
Description: Integrates MobileRider's API with Wordpress
Version: 0.1
Author: Julio C. Menendez Gonzalez
Author URI: http://www.mobilerider.com
License: Copyright @ 2012 MobileRider Networks
*/

define('MOBILERIDER_PREFIX', 'mobilerider_');
define('MOBILERIDER_VERSION', 0.1);
define('MOBILERIDER_DIR', WP_PLUGIN_DIR . '/mobilerider-wordpress/');
define('MOBILERIDER_URL', WP_PLUGIN_URL . '/mobilerider-wordpress/');
define('MOBILERIDER_SHORTCODE', 'mobilerider');

require_once(MOBILERIDER_DIR . 'lib/Mobilerider_Wordpress.php');

if(class_exists('Mobilerider_Wordpress')) {
  $mobileriderWordpress = new Mobilerider_Wordpress();
}